function addTokens(input, tokens){
    if(typeof(input) === "string"){
        if(input.length>=6){
            if(tokens.every(x=> typeof(x.tokenName) !== undefined && typeof(x.tokenName) === "string")){
                if(input.includes("...")){
                    input=input.split("...").filter(x=> x);
                    for(let i=0;i<input.length;i++){
                        input[i]+=`\${${tokens[i].tokenName}}`
                    }
                    input=input.join("");
                }
                return input;
            }
            else{
                throw new Error("Invalid array format");
            }
        }
        else{
            throw new Error("Input should have at least 6 characters");
        }
    }
    else{
        console.log(input + " " + typeof input);
        throw new Error("Invalid input");
    }

    
}

const app = {
    addTokens: addTokens
}

module.exports = app;